package com.example.androidsamplegradlejunitassetj

import org.junit.jupiter.api.*
import org.assertj.core.api.Assertions.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertThat(4).isEqualTo(2+2)
    }
}
